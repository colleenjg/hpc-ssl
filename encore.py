from pathlib import Path
import time

import numpy as np
import torch
from torch import nn, optim
from torch.nn import functional as F
from torch.utils.data import TensorDataset, DataLoader
from torchsummary import summary as tsummary


class Encore(nn.Module):
    
    def __init__(self, n_DG=100, n_CA3=80, n_CA1=100, dim=300, summary=True):
        """
        Initializes an instance of ENCORE (ENtorhinal COmpression-REconstruction hippocampal model). We use pytorch to build an autoencoder-like network.
        Params:
            n_* (int) : Number of units per each layer (Dentate Gyrus or DG, CA3, and CA1).
            dim (int) : Input/output dimension (1-D in our case).
            summary (bool) : If True, it outputs the summary of the model.
        """
        super().__init__()
        
        self.n_DG = n_DG
        self.n_CA3 = n_CA3
        self.n_CA1 = n_CA1
        self.EC_dim = dim

        self.EC_DG = nn.Linear(self.EC_dim, self.n_DG) # relu
        self.DG_CA3 = nn.Linear(self.n_DG, self.n_CA3) # relu
        self.CA3_CA1 = nn.Linear(self.n_CA3, self.n_CA1) # relu
        self.CA1_EC = nn.Linear(self.n_CA1, self.EC_dim) # relu

        if summary:
            tsummary(self, (dim, ))


    def forward(self, x):
        self.DG = F.relu(self.EC_DG(x))
        self.CA3 = F.relu(self.DG_CA3(self.DG))
        self.CA1 = F.relu(self.CA3_CA1(self.CA3))
        out = F.relu(self.CA1_EC(self.CA1))
        return out

            
    def save_model(self, filepath="ENCORE.pth.tar", epoch_n=0, optimizer=None):

        state_dict = {
            "epoch_n": epoch_n,
            "net": "ENCORE",
            "state_dict": self.state_dict(),
        }

        if optimizer is not None:
            state_dict["optimizer"]: optimizer.state_dict()

        torch.save(state_dict, str(filepath)) 
    
    
    def load_model(self, filepath="ENCORE.pth.tar"):
        
        checkpoint = torch.load(filepath, map_location=torch.device("cpu"))
        self.load_state_dict(checkpoint["state_dict"])

        return checkpoint
        

    def run_train(self, data, num_epochs=100, device="cpu", log_freq=10, 
                  filepath=None, *dl_kwargs):

        train_dl, val_df = encore_dls(data, *dl_kwargs)
        train_criterion = nn.MSELoss()
        val_mae = nn.L1Loss() # Mean absolute error

        optimizer = optim.RMSprop(self.parameters(), lr=0.001)
        start_epoch = 0
        if filepath is not None and Path(filepath).is_file():
            checkpoint = self.load(filepath)
            optimizer.load_state_dict(checkpoint["optimizer"])
            start_epoch = checkpoint["epoch_n"]

        self.to(device)

        history = {key: [] for key in ["epoch_n", "val_loss", "val_mae"]}    
        for epoch_n in range(start_epoch, num_epochs + 1):
            self.train()
            for i, (X, ) in enumerate(train_dl):
                if epoch_n == 0: # skip training for epoch 0
                    break

                X.to(device)
                X_pred = self(X)

                # evaluate loss
                loss = train_criterion(X_pred, X)

                # backprop loss
                optimizer.zero_grad()
                loss.backward()

                # step optimizer
                optimizer.step()
                self.zero_grad()
            
            self.eval()
            with torch.no_grad():
                epoch_losses, epoch_maes = [], []
                for i, (X, ) in enumerate(val_df):
                    X.to(device)
                    X_pred = self(X)

                    # evaluate loss and MAE
                    loss = train_criterion(X_pred, X).detach()
                    epoch_losses.append(loss / len(X))
                    
                    mae = val_mae(X_pred, X).detach()
                    epoch_maes.append(mae / len(X))

                history["epoch_n"].append(epoch_n)
                history["val_loss"].append(np.mean(epoch_losses))
                history["val_mae"].append(np.mean(epoch_maes))
            
            # occasionally log to console
            if not epoch_n % log_freq:
                mae = history["val_mae"][-1]
                print(f"Epoch {epoch_n}/{num_epochs}: MAE={mae:.4f}")
        
        if filepath is not None:
            if Path(filepath).is_file():
                print(f"Overwriting {filepath}...")
                time.sleep(5)
            self.save_model(filepath, epoch_n, optimizer)

        return history
    
    
    def test(self, data):

        self.eval()
        with torch.no_grad():
            data = self(torch.Tensor(data))
        
        return data.to("cpu").detach().numpy()
    
    
    def get_output(self, data, layer="DG"):

        if hasattr(self, layer):
            self.test(data)
            return getattr(self, layer).to("cpu").detach().numpy()

        else:
            raise ValueError(f"Layer {layer} not recognized.")


def encore_dls(data, bs=32, prop_val=0.2):

    num_val = int(len(data) * prop_val)
    val_idx = np.sort(np.random.choice(len(data), num_val, replace=False))

    val_mask = np.zeros(len(data), dtype=bool)
    val_mask[val_idx] = True
    train_mask = ~val_mask

    train_dl = DataLoader(
        TensorDataset(torch.Tensor(data[train_mask])), 
        batch_size=bs, 
        shuffle=True
        )

    val_dl = DataLoader(
        TensorDataset(torch.Tensor(data[val_mask])), 
        batch_size=bs,
        shuffle=True
        )

    return train_dl, val_dl

